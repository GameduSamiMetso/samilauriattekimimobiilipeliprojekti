﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class LevelEnd : MonoBehaviour {
    public GameObject endMenu;
    public GameObject pauseButton;
    string tF;

    void Start()
    {
        endMenu.SetActive(false);
        Advertisement.Initialize("1595975");
    }

    void OnTriggerStay2D(Collider2D other)
    {

        Scene currentScene = SceneManager.GetActiveScene();
        string sceneName = currentScene.name;

        Advertisement.Show();
        endMenu.SetActive(true);
        pauseButton.SetActive(false);
        Time.timeScale = 0;
        Debug.Log("Ad is shown.");
        
        if (sceneName == "Level1")
        {
            PlayerPrefs.SetString("Level1Clear", "Clear");
        }
        else if (sceneName == "Level2")
        {
            PlayerPrefs.SetString("Level2Clear", "Clear");
        }
    }
}
