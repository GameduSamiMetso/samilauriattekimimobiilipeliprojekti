﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

    Transform target;
    public float delay = 1f;

    private Vector3 reference = Vector3.zero;

	// Use this for initialization
	void Start () {
        //Locates the player
        target = GameObject.FindWithTag("Player").transform;

	}
	
	// Update is called once per frame
	void FixedUpdate () {

        //Sets the default position of the camera
        Vector2 pos = new Vector2(target.position.x + 10, target.position.y);

        //Makes the camera follow the player smoothly with a small delay
        transform.position = Vector3.SmoothDamp(transform.position, pos,ref reference, delay);

	}
}
