﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IngamePauseButton : MonoBehaviour
{

    public GameObject pauseMenu;
    public AudioSource source;
    public GameObject player;

    private void Start()
    {
        pauseMenu.SetActive(false);
        source = GetComponent<AudioSource>();

    }

    public void TogglePause()
    {
        pauseMenu.SetActive(!pauseMenu.activeSelf);
        if (!pauseMenu.activeSelf == true)
        {
            Time.timeScale = 1.0f;
            source.Play();
        }
        else
        {
            Time.timeScale = 0.0f;
            source.Pause();
        }

    }

    public void ToMenu()
    {
        SceneManager.LoadScene("MainMenu");
        Time.timeScale = 1.0f;
        source.Play();
    }

    public void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Time.timeScale = 1.0f;
        source.Play();
    }

    public void NextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        Time.timeScale = 1.0f;
        source.Play();
    }

    public void LevelSelectScene()
    {
        SceneManager.LoadScene("LevelSelectionScene");
        Time.timeScale = 1.0f;
        source.Play();
    }
}

