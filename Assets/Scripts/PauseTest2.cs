﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseTest2 : MonoBehaviour
{
    public bool IsPaused;
    public Transform canvas;

    void Update()
    {
        Paused();

    }

    void Paused()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (IsPaused)
            {
                Time.timeScale = 1;
                canvas.gameObject.SetActive(false);
                IsPaused = false;
            }
            else
            {
                Time.timeScale = 0;
                canvas.gameObject.SetActive(true);
                IsPaused = true;
            }
        }
    }
}