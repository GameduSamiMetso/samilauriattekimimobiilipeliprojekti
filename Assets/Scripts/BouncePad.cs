﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncePad : MonoBehaviour {

    GameObject player;

    public float force;

	// Use this for initialization
	void Start () {
        player = GameObject.FindWithTag("Player");

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Reloads level when collided with player
        if (collision = player.GetComponent<BoxCollider2D>())
        {

            Vector2 bounceForce = new Vector2(0, force);
            player.GetComponent<Rigidbody2D>().AddForce(bounceForce, ForceMode2D.Impulse);
        }
    }
}
