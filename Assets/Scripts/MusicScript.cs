﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MusicScript : MonoBehaviour {

    AudioSource audio;

    GameObject player;

    // Use this for initialization
    void Start () {

        player = GameObject.FindWithTag("Player");

        audio = GetComponent<AudioSource>();
        audio.Play();
	}

    private void Update()
    { if (player == null)
        {

        }
    else if(player.GetComponent<PlayerScript>().WallDetection() == true || 
            GameObject.FindWithTag("ReloadBox").GetComponent<RestartLevelWhenHit>().CollisionDetection() == true)
        {
            audio.volume--;
        }
    }
}
