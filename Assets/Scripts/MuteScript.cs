﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MuteScript : MonoBehaviour, IPointerClickHandler
{

    string tF;
    public bool mT = true;
    private Toggle tgl;

    void Awake()
    {
        tgl = GetComponent<Toggle>();
        tF = PlayerPrefs.GetString("Mute");
        Debug.Log(tF);
        if (tF == "true")
        {
            mT = true;
            tgl.isOn = true;

        }
        else if (tF == "false")
        {
            mT = false;
            tgl.isOn = false;
        }

    }

    void Update()
    {
        PlayerPrefs.SetString("Mute", tF);
    }


    public void OnPointerClick(PointerEventData Toggle)
    {
        mT = !mT;

        if (mT)
        {
            tF = "true";
           

        }
        else
        {
            tF = "false";
        }

        Debug.Log("toggled: " +tF);
        PlayerPrefs.SetString("Mute", tF);
    }
}