﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{

    public float Thrust = 5f;
    public Rigidbody2D rb;
    public float bulletTimer;

    GameObject mainCamera;

    // Use this for initialization
    void Start()
    {
        
        rb.GetComponent<Rigidbody2D>();

        mainCamera = GameObject.FindWithTag("MainCamera");

        Destroy(gameObject, bulletTimer);
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = Vector2.left * Thrust;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            collision.GetComponent<PlayerScript>().LevelRestart();

            mainCamera.GetComponent<GlitchTriggerer>().TriggerGlitch();

            Destroy(gameObject);
        }
    }
}