﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;

public class DeathPosEvent : MonoBehaviour {


    public void CustomEvent()
    {
        int deathPos = Mathf.RoundToInt(transform.position.x);
        Scene scene = SceneManager.GetActiveScene();
        
        Analytics.CustomEvent("deathPosition", new Dictionary<string, object>
        {
            { "DeathPosX", deathPos + " on level " + scene.name + "."},
        });
    }
}
