﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlitchTriggerMenu : MonoBehaviour
{


    void Start()
    {
        StartCoroutine(GlitchTimer1());
        StartCoroutine(GlitchTimer2());
    }

    IEnumerator GlitchTimer1()
    {


        while (true)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
            audio.pitch = Random.Range(1f, 3f);
            audio.volume = Random.Range(0.1f, 1f);

            GetComponent<Kino.DigitalGlitch>().intensity = Random.Range(0.01f, 0.08f);
            GetComponent<Kino.AnalogGlitch>().scanLineJitter = Random.Range(0.01f, 0.08f);
            GetComponent<Kino.AnalogGlitch>().verticalJump = Random.Range(0.01f, 0.08f);

            yield return new WaitForSeconds(Random.Range(1, 2));

            GetComponent<Kino.DigitalGlitch>().intensity = 0;
            GetComponent<Kino.AnalogGlitch>().scanLineJitter = 0;
            GetComponent<Kino.AnalogGlitch>().verticalJump = 0;

            yield return new WaitForSeconds(Random.Range(3, 6));
        }


    }
    IEnumerator GlitchTimer2()
    {


        while (true)
        {
            GetComponent<Kino.AnalogGlitch>().colorDrift = Random.Range(0.01f, 0.03f);

            yield return new WaitForSeconds(Random.Range(0.5f, 0.75f));

            GetComponent<Kino.AnalogGlitch>().colorDrift = 0;

            yield return new WaitForSeconds(Random.Range(0.5f, 1.5f));
        }
    }
}