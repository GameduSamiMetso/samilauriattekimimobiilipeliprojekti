﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class LevelButtonsScript : MonoBehaviour {

    public GameObject levelButtonPrefab;
    public GameObject levelButtonContainer;
    string lvl1;
    string lvl2;

    private void Start () {
        lvl1 = PlayerPrefs.GetString("Level1Clear");
        lvl2 = PlayerPrefs.GetString("Level2Clear");




        Sprite[] thumbnails = Resources.LoadAll<Sprite>("Levels");
        for (int i = 0; i < thumbnails.Length; i++)
        {

            if(i == 1)
            {
                if(lvl1 != "Clear")
                {
                    break;
                }
            }

            if (i == 2)
            {
                if (lvl2 != "Clear")
                {
                    break;
                }
            }

            GameObject container = Instantiate(levelButtonPrefab) as GameObject;
            container.GetComponent<Image> ().sprite = thumbnails[i];
            container.transform.SetParent(levelButtonContainer.transform,false);

            string sceneName = thumbnails[i].name;
            container.GetComponent<Button> ().onClick.AddListener(() => LoadLevel(sceneName));
        }
	}
    private void LoadLevel(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

}
