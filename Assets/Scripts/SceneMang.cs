﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneMang : MonoBehaviour {
    public string lastScene;

    public void SceneLoad(string scenename)
    {
        SceneManager.LoadScene(scenename);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            BackButton();
        }
    }

    public void BackButton()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        string currentSceneName = currentScene.name;


        if (currentSceneName == "MainMenu")
        {
            Application.Quit();
        }
        else
        {
            SceneManager.LoadScene(lastScene);
        }
    }

}
